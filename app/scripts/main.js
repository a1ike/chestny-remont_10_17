$(document).ready(function () {

  $('.cr-carousel').slick({
    arrows: false,
    nextArrow: '<img src="./images/icon_next.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_prev.png" class="slick-prev">',
    dots: true,
    autoplay: true
  });

  $('.cr-gallery__cards').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_next.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_prev.png" class="slick-prev">',
    dots: false,
    autoplay: true
  });

  $('.cr-reviews__cards').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_next.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_prev.png" class="slick-prev">',
    dots: true,
    autoplay: true
  });

  $('.cr-paper__cards').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_next.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_prev.png" class="slick-prev">',
    dots: false,
    autoplay: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.cr-droper').click(function () {
    $('.cr-nav__this').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  var timer;

  var compareDate = new Date();
  compareDate.setDate(compareDate.getDate() + 7); //just for this demo today + 7 days

  timer = setInterval(function () {
    timeBetweenDates(compareDate);
  }, 1000);

  function timeBetweenDates(toDate) {
    var dateEntered = toDate;
    var now = new Date();
    var difference = dateEntered.getTime() - now.getTime();

    if (difference <= 0) {

      // Timer done
      clearInterval(timer);

    } else {

      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      $('#hours').text(hours);
      $('#minutes').text(minutes);
      $('#seconds').text(seconds);

      $('#hours_2').text(hours);
      $('#minutes_2').text(minutes);
      $('#seconds_2').text(seconds);

      $('#hours_3').text(hours);
      $('#minutes_3').text(minutes);
      $('#seconds_3').text(seconds);
    }
  }

});
